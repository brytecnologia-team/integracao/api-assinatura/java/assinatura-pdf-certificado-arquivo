package br.com.bry.framework.sample.enums;

public enum PosicaoAssinatura {
	
	INFERIOR_DIREITO,
	INFERIOR_ESQUERDO,
	SUPERIOR_DIREITO,
	SUPERIOR_ESQUERDO;

}

package br.com.bry.framework.sample.config;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class SignatureConfig {

	private static final String PATH_SEPARATOR = File.separator;
	
	private static final String RESOURCES_FOLDER = System.getProperty("user.dir") + "" + PATH_SEPARATOR + "src" + PATH_SEPARATOR
	        + "main" + PATH_SEPARATOR + "resources" + PATH_SEPARATOR;
	
	public static final String OUTPUT_RESOURCE_FOLDER = System.getProperty("user.dir") + "" + PATH_SEPARATOR + "src" + PATH_SEPARATOR
	        + "main" + PATH_SEPARATOR + "resources" + PATH_SEPARATOR + "signedDocuments" + PATH_SEPARATOR;
	
	// Available values: "true" and "false".
	public static final String ATTACHED = "true";
	public static final String DETACHED = "false";

	// Available values: "BASIC", "CHAIN", "CHAIN_CRL", "TIMESTAMP", "COMPLETE", "ADRB", "ADRT", "ADRV", "ADRC", "ADRA", "ETSI_B", "ETSI_T", "ETSI_LT" and "ETSI_LTA".
	public static final String PROFILE = "BASIC";

	// Available values: "SHA1", "SHA256" e "SHA512".
	public static final String HASH_ALGORITHM = "SHA256";

	// Available values: "SIGNATURE", "CO_SIGNATURE	" and "COUNTER_SIGNATURE".
	public static final String OPERATION_TYPE = "SIGNATURE";
	
	public static final List<String> ORIGINAL_DOCUMENTS_LOCATION = new ArrayList<>();
	
	public static final String DOCUMENTS_FOLDER = RESOURCES_FOLDER + "documents";
	
	static {
		ORIGINAL_DOCUMENTS_LOCATION.add(DOCUMENTS_FOLDER + PATH_SEPARATOR + "myDocument.pdf");
	}
	
}

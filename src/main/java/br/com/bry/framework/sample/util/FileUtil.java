package br.com.bry.framework.sample.util;

import br.com.bry.framework.sample.config.SignatureConfig;
import io.restassured.RestAssured;
import io.restassured.response.Response;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Scanner;

public class FileUtil {
	
	public static void writeContentToFile(String filePath, String fileName, String downloadLink) throws IOException {

		SimpleDateFormat formatter = new SimpleDateFormat("_yyyy-MM-dd hh.mm.ss.SSS");

		String signedDocumentPath = filePath + fileName + formatter.format(new Date(System.currentTimeMillis())) + ".pdf";
		
		File generatedSignaturesDirectory = new File(filePath);
		generatedSignaturesDirectory.mkdir();
		
		File signedDocumentFile = new File(signedDocumentPath);
		signedDocumentFile.createNewFile();
		
		FileOutputStream fos = new FileOutputStream(signedDocumentFile);
		
		URL url = new URL(downloadLink);
		InputStream is = url.openStream();
		int length = -1;
		byte[] buffer = new byte[1024];
		while((length = is.read(buffer)) != -1) {
			fos.write(buffer, 0, length);
		}
		is.close();
		fos.close();
		fos.flush();

		System.out.println("Successful signature generation: " + signedDocumentPath);
		
	}
	
}

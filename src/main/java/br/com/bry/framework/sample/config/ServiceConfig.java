package br.com.bry.framework.sample.config;

public class ServiceConfig {
	
	private final static String URL_SERVER = "https://hub2.bry.com.br";
	
	private final static String URL_SERVER_PDF = URL_SERVER + "/fw/v1/pdf/pkcs1/assinaturas/acoes/";
	
	public static final String URL_INITIALIZE_SIGNATURE = URL_SERVER_PDF + "inicializar";
	
	public static final String URL_FINALIZE_SIGNATURE = URL_SERVER_PDF + "finalizar";
	
	public static final String ACCESS_TOKEN = "<INSERT_VALID_ACCESS_TOKEN>";
	
}

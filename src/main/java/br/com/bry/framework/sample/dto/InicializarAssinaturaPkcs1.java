package br.com.bry.framework.sample.dto;

public class InicializarAssinaturaPkcs1 {

	private long[] nonces;
	private String perfil;
	private String certificado;
	private String algoritmoHash;
	private String formatoDadosSaida;
	private String formatoDadosEntrada;
	
	public InicializarAssinaturaPkcs1() {}
	
	public InicializarAssinaturaPkcs1(long[] nonces, String perfil, String certificado, String algoritmoHash,
			String formatoDadosSaida, String formatoDadosEntrada) {
		super();
		this.nonces = nonces;
		this.perfil = perfil;
		this.certificado = certificado;
		this.algoritmoHash = algoritmoHash;
		this.formatoDadosSaida = formatoDadosSaida;
		this.formatoDadosEntrada = formatoDadosEntrada;
	}

	public long[] getNonces() {
		return nonces;
	}

	public void setNonces(long[] nonces) {
		this.nonces = nonces;
	}

	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

	public String getCertificado() {
		return certificado;
	}

	public void setCertificado(String certificado) {
		this.certificado = certificado;
	}

	public String getAlgoritmoHash() {
		return algoritmoHash;
	}

	public void setAlgoritmoHash(String algoritmoHash) {
		this.algoritmoHash = algoritmoHash;
	}

	public String getFormatoDadosSaida() {
		return formatoDadosSaida;
	}

	public void setFormatoDadosSaida(String formatoDadosSaida) {
		this.formatoDadosSaida = formatoDadosSaida;
	}

	public String getFormatoDadosEntrada() {
		return formatoDadosEntrada;
	}

	public void setFormatoDadosEntrada(String formatoDadosEntrada) {
		this.formatoDadosEntrada = formatoDadosEntrada;
	}
	
}

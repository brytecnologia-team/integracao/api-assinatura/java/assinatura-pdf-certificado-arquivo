package br.com.bry.framework.sample.dto;

import br.com.bry.framework.sample.enums.Fonte;
import br.com.bry.framework.sample.enums.Rotacao;

public class ConfiguracaoTextoDTO {

	private String texto;
	private Fonte fonte;
	private int tamanhoFonte;
	private boolean incluirCN;
	private boolean incluirCPF;
	private boolean incluirEmail;
	private Rotacao rotacaoTexto;
	
	public ConfiguracaoTextoDTO() {}
	
	public ConfiguracaoTextoDTO(String texto, Fonte fonte, int tamanhoFonte, boolean incluirCN, boolean incluirCPF,
			boolean incluirEmail, Rotacao rotacaoTexto) {
		super();
		this.texto = texto;
		this.fonte = fonte;
		this.tamanhoFonte = tamanhoFonte;
		this.incluirCN = incluirCN;
		this.incluirCPF = incluirCPF;
		this.incluirEmail = incluirEmail;
		this.rotacaoTexto = rotacaoTexto;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public Fonte getFonte() {
		return fonte;
	}

	public void setFonte(Fonte fonte) {
		this.fonte = fonte;
	}

	public int getTamanhoFonte() {
		return tamanhoFonte;
	}

	public void setTamanhoFonte(int tamanhoFonte) {
		this.tamanhoFonte = tamanhoFonte;
	}

	public boolean isIncluirCN() {
		return incluirCN;
	}

	public void setIncluirCN(boolean incluirCN) {
		this.incluirCN = incluirCN;
	}

	public boolean isIncluirCPF() {
		return incluirCPF;
	}

	public void setIncluirCPF(boolean incluirCPF) {
		this.incluirCPF = incluirCPF;
	}

	public boolean isIncluirEmail() {
		return incluirEmail;
	}

	public void setIncluirEmail(boolean incluirEmail) {
		this.incluirEmail = incluirEmail;
	}

	public Rotacao getRotacaoTexto() {
		return rotacaoTexto;
	}

	public void setRotacaoTexto(Rotacao rotacaoTexto) {
		this.rotacaoTexto = rotacaoTexto;
	}
	
}

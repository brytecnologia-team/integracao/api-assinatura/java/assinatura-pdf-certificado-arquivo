package br.com.bry.framework.sample.dto;

public class AssinaturaPkcs1DTO {

	private String nonce;
	private String cifrado;
	
	public AssinaturaPkcs1DTO() {}
	
	public AssinaturaPkcs1DTO(String nonce, String cifrado) {
		super();
		this.nonce = nonce;
		this.cifrado = cifrado;
	}

	public String getNonce() {
		return nonce;
	}

	public void setNonce(String nonce) {
		this.nonce = nonce;
	}

	public String getCifrado() {
		return cifrado;
	}

	public void setCifrado(String cifrado) {
		this.cifrado = cifrado;
	}
}

package br.com.bry.framework.sample;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.stream.IntStream;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.bry.framework.sample.config.CertificateConfig;
import br.com.bry.framework.sample.config.ServiceConfig;
import br.com.bry.framework.sample.config.SignatureConfig;
import br.com.bry.framework.sample.dto.AssinaturaPkcs1DTO;
import br.com.bry.framework.sample.dto.ConfiguracaoImagemDTO;
import br.com.bry.framework.sample.dto.ConfiguracaoTextoDTO;
import br.com.bry.framework.sample.dto.InicializarAssinaturaPkcs1;
import br.com.bry.framework.sample.dto.InitializedData;
import br.com.bry.framework.sample.enums.FormatoDados;
import br.com.bry.framework.sample.enums.HashAlgorithm;
import br.com.bry.framework.sample.enums.PosicaoAssinatura;
import br.com.bry.framework.sample.util.ConverterUtil;
import br.com.bry.framework.sample.util.FileUtil;
import br.com.bry.framework.sample.util.PKCS1DTO;
import br.com.bry.framework.sample.util.Signer;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class BasicSignatureExample {
	
	public static void main(String[] args) throws IOException, JSONException {

		// Step 1 - Load PrivateKey and Certificate
		Signer signer = new Signer(CertificateConfig.PRIVATE_KEY_LOCATION, CertificateConfig.PRIVATE_KEY_PASSWORD);

		// Step 2 - Signature initialization.
		InitializedData initializedData = initializeSignature(signer);

		// Step 3 - Local encryption of signed attributes using private key.
		encryptSignedAttributes(signer, initializedData);

		// Step 4 - Signature finalization.
		List<String> signatureContent = finalizeSignature(signer, initializedData);
		
		for (int i = 0; i < signatureContent.size(); i++)
			FileUtil.writeContentToFile(SignatureConfig.OUTPUT_RESOURCE_FOLDER, "signed-document-" + i, signatureContent.get(0));
		
	}
	
	private static InitializedData initializeSignature(Signer signer) throws IOException, JSONException {
		
		RequestSpecBuilder builder = new RequestSpecBuilder();

		long[] nonces = new long[SignatureConfig.ORIGINAL_DOCUMENTS_LOCATION.size()];
		IntStream.range(0, SignatureConfig.ORIGINAL_DOCUMENTS_LOCATION.size()).forEach(i -> {
			File originalDocumentContent = new File(SignatureConfig.ORIGINAL_DOCUMENTS_LOCATION.get(i));
			builder.addMultiPart("documento", originalDocumentContent);
			nonces[i] = System.currentTimeMillis();
		});
		
		String certificateBase64Content = Base64.getEncoder().encodeToString(signer.getCertificate());

		InicializarAssinaturaPkcs1 dadosInicializar = new InicializarAssinaturaPkcs1();
		dadosInicializar.setNonces(nonces);
		dadosInicializar.setPerfil(SignatureConfig.PROFILE);
		dadosInicializar.setCertificado(certificateBase64Content);
		dadosInicializar.setAlgoritmoHash(SignatureConfig.HASH_ALGORITHM);
		dadosInicializar.setFormatoDadosSaida(FormatoDados.BASE64.name());
		dadosInicializar.setFormatoDadosEntrada(FormatoDados.BASE64.name());
		
		ConfiguracaoImagemDTO configuracaoImagem = new ConfiguracaoImagemDTO();
		configuracaoImagem.setAltura(30);
		configuracaoImagem.setLargura(30);
		configuracaoImagem.setCoordenadaX(10);
		configuracaoImagem.setCoordenadaY(10);
		configuracaoImagem.setPosicao(PosicaoAssinatura.INFERIOR_DIREITO);
		
		ConfiguracaoTextoDTO configuracaoTexto = new ConfiguracaoTextoDTO();
		configuracaoTexto.setTexto("Este documento está digitalmente assinado");
		configuracaoTexto.setTamanhoFonte(11);
		configuracaoTexto.setIncluirCN(true);
		
		RequestSpecification requestSpec = builder.build();
		
		Response initializationResponse = RestAssured.given().auth().preemptive().oauth2(ServiceConfig.ACCESS_TOKEN).spec(requestSpec)
		        .multiPart("dados_inicializar", dadosInicializar)
		        .multiPart("imagem", new File(SignatureConfig.DOCUMENTS_FOLDER + "/imagem.jpg"))
		        .multiPart("imagemFundo", new File(SignatureConfig.DOCUMENTS_FOLDER + "/imagemFundo.jpg"))
		        .multiPart("configuracao_imagem", configuracaoImagem)
		        .multiPart("configuracao_texto", configuracaoTexto)
		        .expect().when().post(ServiceConfig.URL_INITIALIZE_SIGNATURE);
		
		Object responseBody = initializationResponse.getBody().as(Object.class);
		if (initializationResponse.getStatusCode() != 200) {
			
			System.out.println("Error during signature initialization - Status code: " + initializationResponse.getStatusCode());
			System.out.println(responseBody);
			throw new IOException("Error during signature initialization - Signature initialization aborted.");
		}
		
		System.out.println("Signature initialization JSON response: " + responseBody);
		
		JSONObject jsonObject = new JSONObject(ConverterUtil.convertObjectToJSON(responseBody));
		
		JSONArray assinaturasInicializadas = jsonObject.getJSONArray("assinaturasInicializadas");
				
		InitializedData initializedData = new InitializedData();
		initializedData.setNonce(jsonObject.getString("nonce"));
		
		List<PKCS1DTO> data = new ArrayList<>();
		for (int i = 0; i < assinaturasInicializadas.length(); i++) {
			String assinaturaInicializadaStringJson = assinaturasInicializadas.getString(i);

			JSONObject signedAttributesJsonObject = new JSONObject(assinaturaInicializadaStringJson);
			
			PKCS1DTO dado = new PKCS1DTO(signedAttributesJsonObject.getString("nonce"), signedAttributesJsonObject.getString("messageDigest"));
			dado.setOriginalDocument(new File(SignatureConfig.ORIGINAL_DOCUMENTS_LOCATION.get(i)));
			initializedData.getInitializedSignatures().add(dado);
		}
		
		return initializedData;
		
	}
	
	private static void encryptSignedAttributes(Signer signer, InitializedData initializedData) throws IOException {
		
		for (PKCS1DTO dado : initializedData.getInitializedSignatures()) {
			byte[] signatureValue = signer.sign(HashAlgorithm.valueOf(SignatureConfig.HASH_ALGORITHM),
					Base64.getDecoder().decode(dado.getSignedAttribute()));
			dado.setSignatureValue(Base64.getEncoder().encodeToString(signatureValue));
		}
		
	}
	
	private static List<String> finalizeSignature(Signer signer, InitializedData initializedData) throws IOException, JSONException {
		RequestSpecBuilder builder = new RequestSpecBuilder();

		AssinaturaPkcs1DTO[] assinaturasPkcs1DTO = new AssinaturaPkcs1DTO[initializedData.getInitializedSignatures().size()];		
		
		for(int i = 0; i < initializedData.getInitializedSignatures().size(); i++) {
			PKCS1DTO pkcs1Item = initializedData.getInitializedSignatures().get(i);
			AssinaturaPkcs1DTO assinaturaPkcs1DTO = new AssinaturaPkcs1DTO();
			assinaturaPkcs1DTO.setNonce(pkcs1Item.getNonce());
			assinaturaPkcs1DTO.setCifrado(pkcs1Item.getSignatureValue());
			assinaturasPkcs1DTO[i] = assinaturaPkcs1DTO;
		}
		
		RequestSpecification requestSpec = builder.build();

		String certificateBase64Content = Base64.getEncoder().encodeToString(signer.getCertificate());
		
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("nonce", initializedData.getNonce());
		JSONArray jsonArray = new JSONArray();
		for(AssinaturaPkcs1DTO assPkcs1DTO : assinaturasPkcs1DTO) {
			jsonArray.put(new JSONObject(assPkcs1DTO));
		}
		jsonObject.put("assinaturasPkcs1", jsonArray);
		jsonObject.put("formatoDeDados", FormatoDados.BASE64);
		
		System.out.println(jsonObject.toString());
				
		Response finalizationResponse = RestAssured.given().auth().preemptive().oauth2(ServiceConfig.ACCESS_TOKEN).spec(requestSpec)
				.contentType(ContentType.JSON)
		        .body(jsonObject.toString())
		        .expect().when().post(ServiceConfig.URL_FINALIZE_SIGNATURE);
				
		Object responseBody = finalizationResponse.getBody().as(Object.class);
		if (finalizationResponse.getStatusCode() != 200) {
			
			System.out.println("Error during signature finalization - Status code: " + finalizationResponse.getStatusCode());
			System.out.println(responseBody);
			throw new IOException("Error during signature finalization - Signature finalization aborted.");
		}
				
		System.out.println("Signature finalization JSON response: " + responseBody);
		
		JSONObject jsonObjectFinalization = new JSONObject(ConverterUtil.convertObjectToJSON(responseBody));
		
		JSONArray signatureArray = jsonObjectFinalization.getJSONArray("documentos");
		
		List<String> downloadLinks = new ArrayList<>();
		for (int i = 0; i < signatureArray.length(); i++) {
			String signaturesStringJson = signatureArray.getString(i);
			JSONObject signaturesJsonObject = new JSONObject(signaturesStringJson);
			JSONArray links = signaturesJsonObject.getJSONArray("links");
			JSONObject downloadLink = links.getJSONObject(0);
			downloadLinks.add(downloadLink.getString("href"));
		}
		
		return downloadLinks;
	}
	
}

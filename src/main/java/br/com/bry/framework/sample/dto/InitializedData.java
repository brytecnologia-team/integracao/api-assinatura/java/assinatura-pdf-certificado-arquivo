package br.com.bry.framework.sample.dto;

import java.util.ArrayList;
import java.util.List;

import br.com.bry.framework.sample.util.PKCS1DTO;

public class InitializedData {

	private String nonce;
	List<PKCS1DTO> initializedSignatures;
	
	public InitializedData() {
		this.initializedSignatures = new ArrayList<PKCS1DTO>();
	}
	
	public InitializedData(String nonce, List<PKCS1DTO> initializedSignatures) {
		super();
		this.nonce = nonce;
		this.initializedSignatures = initializedSignatures;
	}

	public String getNonce() {
		return nonce;
	}

	public void setNonce(String nonce) {
		this.nonce = nonce;
	}

	public List<PKCS1DTO> getInitializedSignatures() {
		return initializedSignatures;
	}

	public void setInitializedSignatures(List<PKCS1DTO> initializedSignatures) {
		this.initializedSignatures = initializedSignatures;
	}
}

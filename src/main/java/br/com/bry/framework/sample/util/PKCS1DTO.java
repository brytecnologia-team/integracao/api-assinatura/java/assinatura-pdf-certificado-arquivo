package br.com.bry.framework.sample.util;

import java.io.File;

public class PKCS1DTO {

	private String nonce;
	private String signedAttribute;
	private String signatureValue;
	private String hashOriginalDocument;
	private File originalDocument;

	public PKCS1DTO(String nonce, String signedAttribute) {
		this.nonce = nonce;
		this.signedAttribute = signedAttribute;
	}

	public void setSignatureValue(String signatureValue) {
		this.signatureValue = signatureValue;
	}

	public void setOriginalDocument(File originalDocument) {
		this.originalDocument = originalDocument;
	}

	public String getNonce() {
		return nonce;
	}

	public String getSignedAttribute() {
		return signedAttribute;
	}

	public String getSignatureValue() {
		return signatureValue;
	}

	public File getOriginalDocument() {
		return originalDocument;
	}

	public String getHashOriginalDocument() {
		return hashOriginalDocument;
	}

	public void setHashOriginalDocument(String hashOriginalDocument) {
		this.hashOriginalDocument = hashOriginalDocument;
	}

}

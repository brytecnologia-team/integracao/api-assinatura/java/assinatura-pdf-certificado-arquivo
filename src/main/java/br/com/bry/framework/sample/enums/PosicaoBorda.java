package br.com.bry.framework.sample.enums;

public enum PosicaoBorda {

	BORDA_DIREITA,
	BORDA_ESQUERDA,
	BORDA_INFERIOR,
	BORDA_SUPERIOR;
	
}

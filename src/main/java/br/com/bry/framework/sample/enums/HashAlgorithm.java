package br.com.bry.framework.sample.enums;

public enum HashAlgorithm {
	SHA1, SHA256, SHA512
}

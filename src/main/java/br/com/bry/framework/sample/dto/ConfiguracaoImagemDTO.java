package br.com.bry.framework.sample.dto;

import br.com.bry.framework.sample.enums.PaginaAssinatura;
import br.com.bry.framework.sample.enums.PosicaoAssinatura;
import br.com.bry.framework.sample.enums.Rotacao;

public class ConfiguracaoImagemDTO {

	private int altura;
	private int largura;
	private int coordenadaX;
	private int coordenadaY;
	private PosicaoAssinatura posicao;
	private PaginaAssinatura pagina;
	private int numeroPagina;
	private String campoAssinatura;
	private Rotacao rotacaoImagemBorda;
	private boolean imagemInvertida;
	private int proporcaoImagem;
	
	public ConfiguracaoImagemDTO() {}
	
	public ConfiguracaoImagemDTO(int altura, int largura, int coordenadaX, int coordenadaY, PosicaoAssinatura posicao,
			PaginaAssinatura pagina, int numeroPagina, String campoAssinatura, Rotacao rotacaoImagemBorda,
			boolean imagemInvertida, int proporcaoImagem) {
		super();
		this.altura = altura;
		this.largura = largura;
		this.coordenadaX = coordenadaX;
		this.coordenadaY = coordenadaY;
		this.posicao = posicao;
		this.pagina = pagina;
		this.numeroPagina = numeroPagina;
		this.campoAssinatura = campoAssinatura;
		this.rotacaoImagemBorda = rotacaoImagemBorda;
		this.imagemInvertida = imagemInvertida;
		this.proporcaoImagem = proporcaoImagem;
	}

	public int getAltura() {
		return altura;
	}

	public void setAltura(int altura) {
		this.altura = altura;
	}

	public int getLargura() {
		return largura;
	}

	public void setLargura(int largura) {
		this.largura = largura;
	}

	public int getCoordenadaX() {
		return coordenadaX;
	}

	public void setCoordenadaX(int coordenadaX) {
		this.coordenadaX = coordenadaX;
	}

	public int getCoordenadaY() {
		return coordenadaY;
	}

	public void setCoordenadaY(int coordenadaY) {
		this.coordenadaY = coordenadaY;
	}

	public PosicaoAssinatura getPosicao() {
		return posicao;
	}

	public void setPosicao(PosicaoAssinatura posicao) {
		this.posicao = posicao;
	}

	public PaginaAssinatura getPagina() {
		return pagina;
	}

	public void setPagina(PaginaAssinatura pagina) {
		this.pagina = pagina;
	}

	public int getNumeroPagina() {
		return numeroPagina;
	}

	public void setNumeroPagina(int numeroPagina) {
		this.numeroPagina = numeroPagina;
	}

	public String getCampoAssinatura() {
		return campoAssinatura;
	}

	public void setCampoAssinatura(String campoAssinatura) {
		this.campoAssinatura = campoAssinatura;
	}

	public Rotacao getRotacaoImagemBorda() {
		return rotacaoImagemBorda;
	}

	public void setRotacaoImagemBorda(Rotacao rotacaoImagemBorda) {
		this.rotacaoImagemBorda = rotacaoImagemBorda;
	}

	public boolean isImagemInvertida() {
		return imagemInvertida;
	}

	public void setImagemInvertida(boolean imagemInvertida) {
		this.imagemInvertida = imagemInvertida;
	}

	public int getProporcaoImagem() {
		return proporcaoImagem;
	}

	public void setProporcaoImagem(int proporcaoImagem) {
		this.proporcaoImagem = proporcaoImagem;
	}
	
}
